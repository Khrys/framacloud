# Installation de Dolomon

Dolomon compte combien de fois un lien a été visité.

Cela veut dire qu’il peut vous dire combien de fois un fichier a été téléchargé (il a été programmé pour cet usage). Mais vous pouvez aussi l’utiliser pour suivre les visites d’une page.

## Prérequis

Commençons par installer quelques paquets :

```
sudo apt-get install build-essential libssl-dev libpq-dev  zlib1g-dev cpanminus git
```

Dolomon est codé en Perl, pour le faire fonctionner il est nécessaire d’installer Carton, un gestionnaire de modules Perl.

```
cpan Carton  inc::Module::Install
```

## Installation

```
git clone https://framagit.org/luc/dolomon.git
cd dolomon
carton install
cp dolomon.conf.template dolomon.conf
vi dolomon.conf
```

Le fichier dolomon.conf.template est documenté. Merci d'ouvrir un [ticket](https://framagit.org/luc/dolomon/issues) si quelque-chose n'est pas clair.

## Créer une base de données

Sur le serveur qui héberge PostgreSQL :

```
su - postgres
createuser dolomon_user -P
createdb -O dolomon_user dolomon_db
createuser dolomon_minion_user -P
createdb -O dolomon_minion_user dolomon_minion_db
psql dolomon_db
CREATE EXTENSION "uuid-ossp";
\q
exit
```

Si vous voulez installer PostgreSQL sur votre serveur :
```
sudo apt-get install postgresql-server
```

## Systemd

```
cp utilities/dolomon*.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable dolomon.service dolomon-minion@1.service
systemctl start dolomon.service dolomon-minion@1.service
```

Notez que vous pouvez ajouter des minions avec :
```
systemctl enable dolomon-minion@2.service
systemctl start dolomon-minion@2.service
```

Ajoutez des minions si la file des tâches augmente plus rapidement que les tâches traitées. En tant qu'admin, vous pouvez voir l'état de la file de tâche dans `/admin/minion`.

## Nginx

```
cp utilities/dolomon.nginx.conf /etc/nginx/sites-available/dolomon.conf
ln -s /etc/nginx/sites-available/dolomon.conf /etc/nginx/sites-enabled/dolomon.conf
vi /etc/nginx/sites-available/dolomon.conf
nginx -t && nginx -s reload
```

# Contribution

Merci d'ouvrir des tickets sur https://framagit.org/luc/dolomon/issues.

## Soumettre un correctif

Merci de toujours travailler sur la branche « développement » si elle existe (c'est le commencement de Dolomon, il n'y a pas encore de branche de développement donc vous pouvez travailler sur la branche principale « master », mais cela changera avec sa première version officielle)

## Ajouter une nouvelle langue

Allez sur https://trad.framasoft.org/, créez un compte, rejoignez l'équipe de traduction que vous souhaitez ou ouvrez un [ticket](https://framagit.org/luc/dolomon/issues) pour être ajouté au projet Dolomon et commencez à traduire sur https://trad.framasoft.org/zanata/project/view/dolomon. Si la langue que vous voulez ajouter n'a pas d'équipe, veuillez ouvrir un [ticket](https://framagit.org/luc/dolomon/issues).

# Créer un nouveau thème

Rendez-vous dans le répertoire de Dolomon et faites :

````
carton exec ./script/dolomon theme name_of_your_new_theme
````

Cela créera un squelette dans le dossier `themes`.

Si vous créez un fichier avec le même nom et le même chemin d'accès que celui du thème par défaut, il prévaudra.
Si vous créez un nouveau fichier, il sera disponible.

Si vous avez ajouté de nouvelles chaînes à traduire, allez dans 'themes/name_of_your_new_theme' et faites :

````
make locales
````

Vous trouverez les fichiers de traduction de vos chaînes dans 'themes/name_of_your_new_theme/lib/Dolomon/I18N'
